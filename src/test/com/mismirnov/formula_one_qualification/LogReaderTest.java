package com.mismirnov.formula_one_qualification;

import com.mismirnov.formula_one_qualification.reader.LogReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.unitils.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class LogReaderTest {

    private LogReader logReader;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setUp() {
        logReader = new LogReader();
    }

    @Test
    public void givenFileWithTimeLogs_whenRead_thenMapContainingFormattedResultIsReturned() throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        Map<String, Duration> expected = new TreeMap();
        expected.put("SVM", Duration.parse("PT1M12.463S"));

        Map<String, Duration> actual = logReader.read("src/test/resources/start.log", "src/test/resources/end.log");

        Assert.assertEquals(actual, expected);
    }
}

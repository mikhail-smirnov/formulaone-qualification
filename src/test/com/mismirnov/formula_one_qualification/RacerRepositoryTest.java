package com.mismirnov.formula_one_qualification;

import com.mismirnov.formula_one_qualification.racer.Racer;
import com.mismirnov.formula_one_qualification.racer.RacerRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.unitils.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;



public class RacerRepositoryTest {

    RacerRepository racerRepository;

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setUp() {
        racerRepository = new RacerRepository();
    }

    @Test
    public void givenFileWithAbbreviations_whenRead_thenMapContainingFormattedResultIsReturned() throws IOException {
        List<Racer> expected = Arrays.asList(new Racer("SPF","Sergio Perez","FORCE INDIA MERCEDES",Duration.parse("PT1M12.355S")));
        Map<String, Duration> lapTimes = new LinkedHashMap<>();
        lapTimes.put("SPF",Duration.parse("PT1M12.355S"));

        List<Racer> actual = racerRepository.getRacers("src/test/resources/racertestfile.log", lapTimes);

        assertReflectionEquals(actual, expected);
    }
}

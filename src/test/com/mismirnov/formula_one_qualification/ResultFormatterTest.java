package com.mismirnov.formula_one_qualification;

import com.mismirnov.formula_one_qualification.formatter.ResultFormatter;
import com.mismirnov.formula_one_qualification.racer.Racer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.lineSeparator;

public class ResultFormatterTest {

    ResultFormatter resultFormatter;

    @Before
    public void setUp() {
        resultFormatter = new ResultFormatter();
    }

    @Test
    public void givenLimitExceedsTheNumberOfRacers_whenFormat_thenFormattedStringWithoutSeparationLineIsReturned() {
        StringBuilder expected = new StringBuilder();
        expected.append("Carlos Sainz").append("|").append("RENAULT                  ").append("|").append("01:50.198").append(lineSeparator());
        expected.append("Sergio Perez").append("|").append("FORCE INDIA MERCEDES     ").append("|").append("02:50.198").append(lineSeparator());
        expected.append("Pierre Gasly").append("|").append("SCUDERIA TORO ROSSO HONDA").append("|").append("03:50.198").append(lineSeparator());
        List<Racer> racers = Arrays.asList(new Racer("CSR", "Carlos Sainz", "RENAULT", Duration.parse("PT1M50.198S")), new Racer("SPF", "Sergio Perez", "FORCE INDIA MERCEDES", Duration.parse("PT2M50.198S")), new Racer("PGS", "Pierre Gasly", "SCUDERIA TORO ROSSO HONDA", Duration.parse("PT3M50.198S")));

        String actual = resultFormatter.format(racers, 15);

        Assert.assertEquals(expected.toString(), actual);
    }

    @Test
    public void givenLimitIsBelowTheNumberOfRacers_whenFormat_thenFormattedStringWithSeparationLineIsReturned() {
        StringBuilder expected = new StringBuilder();
        expected.append("Carlos Sainz").append("|").append("RENAULT                  ").append("|").append("01:50.198").append(lineSeparator());
        expected.append("Sergio Perez").append("|").append("FORCE INDIA MERCEDES     ").append("|").append("02:50.198").append(lineSeparator());
        expected.append("------------------------------------------------").append(lineSeparator());
        expected.append("Pierre Gasly").append("|").append("SCUDERIA TORO ROSSO HONDA").append("|").append("03:50.198").append(lineSeparator());
        List<Racer> racers = Arrays.asList(new Racer("CSR", "Carlos Sainz", "RENAULT", Duration.parse("PT1M50.198S")), new Racer("SPF", "Sergio Perez", "FORCE INDIA MERCEDES", Duration.parse("PT2M50.198S")), new Racer("PGS", "Pierre Gasly", "SCUDERIA TORO ROSSO HONDA", Duration.parse("PT3M50.198S")));

        String actual = resultFormatter.format(racers, 2);

        Assert.assertEquals(expected.toString(), actual);
    }
}

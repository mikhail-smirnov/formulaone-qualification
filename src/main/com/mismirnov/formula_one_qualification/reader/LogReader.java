package com.mismirnov.formula_one_qualification.reader;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogReader {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss.SSS");

    public Map<String, Duration> read(String filePathStartLog, String filePathEndLog) {
        try (Stream<String> startLogStream = Files.lines(Paths.get(filePathStartLog));
             Stream<String> endLogStream = Files.lines(Paths.get(filePathEndLog))
        ) {
            Map<String, LocalDateTime> startLogMap = readLog(startLogStream);
            Map<String, LocalDateTime> endLogMap = readLog(endLogStream);
            return startLogMap.entrySet().stream().collect(Collectors.toMap(
                    Map.Entry::getKey,
                    entry -> Duration.between(entry.getValue(), endLogMap.get(entry.getKey()))
            ));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Map<String, LocalDateTime> readLog(Stream<String> inputLog) {
        return inputLog.map(str -> str.split("(?=\\d)", 2)).collect(
                Collectors.toMap(key -> key[0], val -> LocalDateTime.parse(val[1], formatter), (v1, v2) -> v1));
    }
}

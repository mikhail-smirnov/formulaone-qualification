package com.mismirnov.formula_one_qualification;

import com.mismirnov.formula_one_qualification.racer.Racer;
import com.mismirnov.formula_one_qualification.racer.RacerRepository;
import com.mismirnov.formula_one_qualification.reader.LogReader;
import com.mismirnov.formula_one_qualification.formatter.ResultFormatter;

import java.time.Duration;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        RacerRepository racerRepository = new RacerRepository();
        LogReader logReader = new LogReader();
        Map<String, Duration> logMap = logReader.read("src/main/resources/start.log", "src/main/resources/end.log");
        List<Racer> racersList = racerRepository.getRacers("src/main/resources/abbreviations.txt", logMap);
        ResultFormatter resultFormatter = new ResultFormatter();
        System.out.println(resultFormatter.format(racersList, 15));
    }
}

package com.mismirnov.formula_one_qualification.formatter;

import com.mismirnov.formula_one_qualification.racer.Racer;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static java.lang.System.lineSeparator;

public class ResultFormatter {

    public String format(List<Racer> unformattedResultList, int limit) {
        int nameMaxLength = findMaxLength(unformattedResultList, Racer::getName);
        int teamMaxLength = findMaxLength(unformattedResultList, Racer::getTeam);
        int timeMaxLength = findMaxLength(unformattedResultList, racer -> racer.getBestLapTime().toString());
        StringBuilder formattedResult = new StringBuilder();
        AtomicInteger counter = new AtomicInteger(0);
        unformattedResultList.stream().map(racer -> limit != counter.incrementAndGet() ? printRacer(racer, nameMaxLength, teamMaxLength) : printRacer(racer, nameMaxLength, teamMaxLength) + repeatChar('-', nameMaxLength + teamMaxLength + timeMaxLength) + lineSeparator())
                .forEach(formattedResult::append);
        return formattedResult.toString();

    }

    private String printRacer(Racer racer, int nameMaxLength, int teamMaxLength) {
        return String.format("%-" + nameMaxLength + "s" + "|" + "%-" + teamMaxLength + "s" + "|%s" + lineSeparator(), racer.getName(), racer.getTeam(), formatBestLapTime(racer.getBestLapTime()));
    }

    private String repeatChar(char character, int times) {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < times; i++) {
            string.append(character);
        }
        return string.toString();
    }

    private int findMaxLength(List<Racer> racers, Function<Racer, String> racerGetter) {
        return racers.stream().map(racerGetter::apply)
                .mapToInt(String::length).max().orElse(0);
    }

    private String formatBestLapTime(Duration bestLapTime) {
        long minutes = bestLapTime.toMinutes();
        long seconds = bestLapTime.minusMinutes(minutes).getSeconds();
        long millis = bestLapTime.minusMinutes(minutes).minusSeconds(seconds).toMillis();
        return String.format("%02d:%02d.%03d", minutes, seconds, millis);
    }
}

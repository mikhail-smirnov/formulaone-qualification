package com.mismirnov.formula_one_qualification.racer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;

import static java.util.stream.Collectors.toList;

import java.util.stream.Stream;

public class RacerRepository {

    public List<Racer> getRacers(String filePath, Map<String, Duration> logMap) {
        List<Racer> racers = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            racers = stream.map(line -> line.split("_"))
                    .map(line -> new Racer(line[0], line[1], line[2], logMap.get(line[0]))).sorted(Comparator.comparing(Racer::getBestLapTime))
                    .collect(toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return racers;
    }
}



package com.mismirnov.formula_one_qualification.racer;

import java.time.Duration;

public class Racer {

    private String abbreviation;
    private String name;
    private String team;
    private Duration bestLapTime;

    public Racer(String abbreviation, String name, String team, Duration bestLapTime) {
        this.abbreviation = abbreviation;
        this.name = name;
        this.team = team;
        this.bestLapTime = bestLapTime;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getName() {
        return name;
    }

    public String getTeam() {
        return team;
    }

    public Duration getBestLapTime() {
        return bestLapTime;
    }
}
